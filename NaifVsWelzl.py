import sys
import time
from math import sqrt
from random import randint, shuffle
import copy
import os
import matplotlib.pyplot as plt
import numpy as np

INF = 1e18

class Point:
    def __init__(self, X=0, Y=0) -> None:
        self.X = X
        self.Y = Y

class Circle:
    def __init__(self, c=Point(), r=0) -> None:
        self.C = c
        self.R = r

def dist(a, b):
    return int(sqrt(pow(a.X - b.X, 2) + pow(a.Y - b.Y, 2)))


def is_inside(c, p):
    return dist(c.C, p) <= c.R

def get_circle_center(bx, by, cx, cy):
    B = bx * bx + by * by
    C = cx * cx + cy * cy
    D = bx * cy - by * cx
    return Point((cy * B - by * C) / (2 * D), (bx * C - cx * B) / (2 * D))

def circle_from1(A, B):
    C = Point((A.X + B.X) / 2.0, (A.Y + B.Y) / 2.0 )
    return Circle(C, dist(A, B) / 2.0 )


def circle_from2(A, B, C):
    I = get_circle_center(B.X - A.X, B.Y - A.Y, C.X - A.X, C.Y - A.Y)

    I.X += A.X
    I.Y += A.Y
    return Circle(I, dist(I, A))


def is_valid_circle(c, P):

    for p in P:
        if not is_inside(c, p):
            return False
    return True


def min_circle_trivial(P):
    assert(len(P) <= 3)
    if not P:
        return Circle()
    elif len(P) == 1:
        return Circle(P[0], 0)
    elif len(P) == 2:
        return circle_from1(P[0], P[1])

    for i in range(3):
        for j in range(i + 1, 3):
            c = circle_from1(P[i], P[j])
            if is_valid_circle(c, P):
                return c

    return circle_from2(P[0], P[1], P[2])

def welzl_helper(P, R, n):

    if n == 0 or len(R) == 3:
        return min_circle_trivial(R)
    
    idx = randint(0, n-1)
    p = P[idx]
 
    P[idx], P[n - 1] = P[n-1], P[idx]

    d = welzl_helper(P, R.copy(), n - 1)

    if is_inside(d, p):
        return d

    R.append(p)

    return welzl_helper(P, R.copy(), n - 1)
def welzl(P):

    P_copy = P.copy()
    shuffle(P_copy)
    return welzl_helper(P_copy, [], len(P_copy))

def read_points_from_file(file_path):
    points = []
    with open(file_path, 'r') as file:
        for line in file:
            try:
                words = line.split()

                if len(words) >= 2:
                    x, y = map(int, words[:2])
                    points.append(Point(x, y))
                else:
                    print(f"Ignoring invalid line (not enough words): {line.strip()}")
            except ValueError:
                print(f"Ignoring invalid line: {line.strip()}")

    return points
def tme1exercice4(input_points):
    points = copy.deepcopy(input_points)
    if len(points) < 1:
        return None

    cX, cY, cRadius, cRadiusSquared = 0.0, 0.0, 0.0, 0.0

    for p in points:
        for q in points:
            cX = 0.5 * (p.X + q.X)
            cY = 0.5 * (p.Y + q.Y)
            cRadiusSquared = 0.25 * ((p.X - q.X) ** 2 + (p.Y - q.Y) ** 2)
            all_hit = True
            for s in points : 
                if (s.X - cX) ** 2 + (s.Y - cY) ** 2 <= cRadiusSquared:
                    all_hit = False
                    break
            if all_hit:
                return Circle(Point(cX,cY), int(cRadiusSquared ** 0.5))

    resX, resY, resRadiusSquared = 0.0, 0.0, float('inf')

    for i in range(len(points)):
        for j in range(i + 1, len(points)):
            for k in range(j + 1, len(points)):
                p, q, r = points[i], points[j], points[k]

                if (q.X - p.X) * (r.Y - p.Y) - (q.Y - p.Y) * (r.X - p.X) == 0:
                    continue
                if p.Y == q.Y or p.Y == r.Y:
                    if p.Y == q.Y:
                        p, r = points[k], points[i]
                    else:
                        p, q = points[j], points[i]

                mX, mY = 0.5 * (p.X + q.X), 0.5 * (p.Y + q.Y)
                nX, nY = 0.5 * (p.X + r.X), 0.5 * (p.Y + r.Y)

                alpha1 = (q.X - p.X) / (p.Y - q.Y)
                beta1 = mY - alpha1 * mX
                alpha2 = (r.X - p.X) / (p.Y - r.Y)
                beta2 = nY - alpha2 * nX

                cX = (beta2 - beta1) / (alpha1 - alpha2)
                cY = alpha1 * cX + beta1
                cRadiusSquared = (p.X - cX) ** 2 + (p.Y - cY) ** 2

                if cRadiusSquared >= resRadiusSquared:
                    continue

                all_hit = all((s.X - cX) ** 2 + (s.Y - cY) ** 2 <= cRadiusSquared for s in points)
                if all_hit:
                    resX, resY, resRadiusSquared = cX, cY, cRadiusSquared

    return Circle(Point(resX, resY), resRadiusSquared ** 0.5)
def isValid(points, circle):
    pointC = Point(circle.C.X, circle.C.Y)
    for p in points:
        
        if dist(p, pointC) > circle.R:
            print(p.X,"," , p.Y, "," , circle.R)
            return False
    return True

def plot_runtimes(welzl_runtimes, naive_runtimes):
    algorithms = ['Welzl', 'Naive']

    average_runtimes = [sum(welzl_runtimes) / len(welzl_runtimes), sum(naive_runtimes) / len(naive_runtimes)]

    std_devs = [np.std(welzl_runtimes), np.std(naive_runtimes)]

    x_pos = np.arange(len(algorithms))

    fig, ax = plt.subplots()
    bar_width = 0.35
    opacity = 0.8

    rects1 = ax.bar(x_pos, average_runtimes, bar_width, label='Average Runtime', yerr=std_devs, capsize=5, alpha=opacity)

    ax.set_xlabel('Algorithm')
    ax.set_ylabel('Time (seconds)')
    ax.set_title('Comparison of Average Runtimes between Welzl and Naive Algorithms')
    ax.set_xticks(x_pos)
    ax.set_xticklabels(algorithms)
    ax.legend()

    plt.tight_layout()
    plt.show()

def run_algorithm(file_path, algorithm_function, name, fileName):
    input_points = read_points_from_file(file_path)
    
    pointslen = len(input_points) * 2
    sys.setrecursionlimit(pointslen)


    print(f"Starting {name} Alorithme")

    start_time = time.time()
    result_circle = algorithm_function(input_points)
    end_time = time.time()

    elapsed_time = end_time - start_time

    print(f"\nElapsed Time for {fileName} {elapsed_time} seconds, the algorithm processed {len(input_points)} points as input")
    print("Center =", result_circle.C.X, ",", result_circle.C.Y, "Radius =", result_circle.R)
    print(isValid(input_points, result_circle))

    return elapsed_time

if __name__ == '__main__':
    folder_path = 'C:\\Users\\musta\\OneDrive\\Bureau\\Project\\samples'
    files = os.listdir(folder_path)

    num_tests = len(files) 

    welzl_runtimes = []
    naive_runtimes = []

    for index, point_file in enumerate(files):
        file_path = os.path.join(folder_path, point_file)
        
        # Welzl
        welzl_runtime = run_algorithm(file_path, welzl, "Welzl", point_file)
        welzl_runtimes.append(welzl_runtime)

        print("-------------------------------------------")

        # Naive
        naive_runtime = run_algorithm(file_path, tme1exercice4, "Naive", point_file)
        naive_runtimes.append(naive_runtime)

        print("-------------------------------------------")

        files_remaining = num_tests - index - 1
        print(f"\nRemaining files to process: {files_remaining}")

        print("-------------------------------------------")


    # Calculer la moyenne des temps d'exécution pour chaque algorithme
    welzl_average_runtime = sum(welzl_runtimes) / len(welzl_runtimes)
    naive_average_runtime = sum(naive_runtimes) / len(naive_runtimes)

    print(f"\nTotal number of tests performed: {num_tests}")
    print(f"Average Runtime for Welzl over all tests: {welzl_average_runtime} seconds")
    print(f"Average Runtime for Naive over all tests: {naive_average_runtime} seconds")
    
    plot_runtimes(welzl_runtimes, naive_runtimes)


